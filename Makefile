DEPS=include/principal.h

bin/procesador_paralelo: obj/main.o $(DEPS)
	mkdir -p bin
	gcc -o bin/procesador_paralelo obj/main.o

obj/main.o: src/main.c $(DEPS)
	mkdir -p obj
	gcc -c src/main.c -o obj/main.o

.PHONY: clean

clean: 
	rm obj/*.o bin/procesador_paralelo
