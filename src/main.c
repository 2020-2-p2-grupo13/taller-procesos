#include "../include/principal.h"


int main(int argc, char **argv){

    char adder[] = "_bw";
    char procesador[] = "./procesador_png";

    char buffer[MAX] = {0};

    char filename[MAX] = {0};
    char extension[MAX] = {0};

    char resultado[MAX] = {0};

    pid_t pids[MAX];

    if(argc <= 1) 
        perror("Envie las imagenes a procesar\n");

    for(int i= 0; i<argc-1;i++){
        pid_t pid = fork();
        if(pid == 0){
            snprintf(resultado, MAX, "%d.png", (i+1));
            char *argv2[] = {procesador,argv[i+1],resultado, NULL};
            execvp(procesador,argv2);
            return -1;
        }else{
            pids[i] = pid;
        }
    }
    int status;

    for(int i= 0; i<argc-1;i++){
        if (waitpid(pids[i],&status,0) >= 0){
            printf("%s (archivo BW %d.png): status %d\n",argv[i+1], (i+1), WEXITSTATUS(status));
        }       
    }

    
}